package ku.cs.service;

import ku.cs.model.*;

import java.util.ArrayList;

public class OrderHardcodeDataSource {
    private OrderList orderList;

    public OrderHardcodeDataSource() {
        this.orderList = new OrderList();
//        hardcode();
    }

//    private void hardcode() {
//        orderList.addOrder(new Order("000001","111111",new ArrayList<>(),1,1000,"AOM","Not paid","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000002","111111",new ArrayList<>(),1,1000,"AOM","Not paid","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000003","111111",new ArrayList<>(),1,1000,"AOM","Waiting","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000004","111111",new ArrayList<>(),1,1000,"AOM","Waiting","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000005","111111",new ArrayList<>(),1,1000,"AOM","Waiting","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000006","111111",new ArrayList<>(),1,1000,"AOM","Paid","Not enter","Incomplete"));
//        orderList.addOrder(new Order("000007","111111",new ArrayList<>(),1,1000,"AOM","Paid","In process","Incomplete"));
//        orderList.addOrder(new Order("000008","111111",new ArrayList<>(),1,1000,"AOM","Paid","In process","Incomplete"));
//        orderList.addOrder(new Order("000009","111111",new ArrayList<>(),1,1000,"AOM","Paid","Finished","Incomplete"));
//        orderList.addOrder(new Order("000010","111111",new ArrayList<>(),1,1000,"AOM","Paid","Finished","Complete"));
//        orderList.addOrder(new Order("000011","111111",new ArrayList<>(),1,1000,"AOM","Paid","Finished","Complete"));
//    }

    public OrderList readData(){
        return this.orderList;
    }

    public void writeData(OrderList orderList){
        this.orderList = orderList;
    }
}
