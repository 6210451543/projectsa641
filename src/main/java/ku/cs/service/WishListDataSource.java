package ku.cs.service;

import ku.cs.model.Product;
import ku.cs.model.WishList;

import java.io.*;
import java.util.HashMap;

public class WishListDataSource {
    private final String fileDirectoryName;
    private final String fileName;
    private WishList wishList;

    public WishListDataSource(String fileDirectoryName, String fileName) {
        this.fileDirectoryName = fileDirectoryName;
        this.fileName = fileName;
        checkFileIsExisted();
    }

    private void checkFileIsExisted() {
        File file = new File(fileDirectoryName);
        if (!file.exists()) {
            file.mkdirs();
        }
        String filePath = fileDirectoryName + File.separator + fileName;
        file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readData() {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);

        FileReader fileReader;
        BufferedReader bufferedReader;
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);

            String line ;
            while ( ((line = bufferedReader.readLine()) != null)){
                String[] data = line.split(",");
                wishList.addWishlist(new Product(data[0],data[1],Double.parseDouble(data[2])
                        ,Double.parseDouble(data[3]),data[4]),Integer.parseInt(data[5]));

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public WishList getData() {
        wishList = new WishList();
        readData();
        return wishList;
    }

    public void setData(WishList wishList) {
        String filePath = fileDirectoryName + File.separator + fileName;
        File file = new File(filePath);

        FileWriter fileWriter = null;
        BufferedWriter buffer = null;
        try {
            fileWriter = new FileWriter(file);
            buffer = new BufferedWriter(fileWriter);
            buffer.write(wishList.toCsv());
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                assert buffer != null;
                buffer.close();
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
