package ku.cs.model;

import java.util.HashMap;

public class WishList {
    private HashMap<Product,Integer> wishList;

    public WishList() {
        wishList = new HashMap<>();
    }
    public void addWishlist(Product product, int quantity){
        wishList.put(product,quantity);
    }
    public void getProduct(){
    }

    public void clear(){
        for(Product product:wishList.keySet()){
            wishList.remove(product);
        }
    }
    public HashMap<Product, Integer> getWishList() {
        return wishList;
    }
    public String toCsv() {
        StringBuilder result = new StringBuilder();
        for(Product product:wishList.keySet()){
            result.append(product.toCsv()).append(",").append(wishList.get(product)).append("\n");
        }
        return result.toString();
    }
}
