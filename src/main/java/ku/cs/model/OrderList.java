package ku.cs.model;

import java.util.ArrayList;

public class OrderList {

    private ArrayList<Order> orders;
    private Order order;


    public OrderList() {
        this.orders = new ArrayList<>();
    }
    public void addOrder(Order order){
        orders.add(order);
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public Order getOrder() {
        return order;
    }
}
