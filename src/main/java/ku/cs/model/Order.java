package ku.cs.model;

import java.util.ArrayList;

public class Order {

    private String orderID;
    private double totalPrice;
    private String customerUsername;
    private String checkoutStatus;
    private String productionStatus;
    private String orderStatus;

    public Order(String orderID, double totalPrice, String customerUsername, String checkoutStatus, String productionStatus, String orderStatus) {
        this.orderID = orderID;
        this.totalPrice = totalPrice;
        this.customerUsername = customerUsername;
        this.checkoutStatus = checkoutStatus;
        this.productionStatus = productionStatus;
        this.orderStatus = orderStatus;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCustomer() {
        return customerUsername;
    }

    public void setCustomer(String customer) {
        this.customerUsername = customer;
    }

    public String getCheckoutStatus() {
        return checkoutStatus;
    }

    public void setCheckoutStatus(String checkoutStatus) {
        this.checkoutStatus = checkoutStatus;
    }

    public String getProductionStatus() {
        return productionStatus;
    }

    public void setProductionStatus(String productionStatus) {
        this.productionStatus = productionStatus;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
