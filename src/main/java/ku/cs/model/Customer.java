package ku.cs.model;

public class Customer extends User{

    private String customerName;
    private String customerTel;
    private String customerEmail;
    private String customerAddress;

    public Customer(String userName, String userPassword, String customerName ,String customerTel, String customerEmail, String customerAddress) {
        super(userName, userPassword);
        this.customerName = customerName;
        this.customerTel = customerTel;
        this.customerEmail = customerEmail;
        this.customerAddress = customerAddress;
    }

    @Override
    public String getUserName() {
        return super.getUserName();
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public String getCustomerTel() {
        return customerTel;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public String getCustomerName() {
        return customerName;
    }
}
