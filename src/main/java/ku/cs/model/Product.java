package ku.cs.model;

public class Product {
    private String productID;
    private String productName;
    private double productPrice;
    private double stockOfProduct;
    private String productionTime;

    public Product(String productID, String productName, double productPrice, double stockOfProduct, String productionTime){
        this.productID = productID;
        this.productName = productName;
        this.productPrice = productPrice;
        this.stockOfProduct = stockOfProduct;
        this.productionTime = productionTime;
    }

    public String getProductID() {
        return productID;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public double getStockOfProduct() {
        return stockOfProduct;
    }

    public String getProductionTime() {
        return productionTime;
    }

    public String toCsv(){
        return getProductID()+","+getProductName()+","+getProductPrice()+","+getStockOfProduct()+","+getProductionTime();
    }
}
