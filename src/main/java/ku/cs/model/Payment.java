package ku.cs.model;

public class Payment {
    private String payOrder;
    private String payMethod;
    private String payAmount;
    private String payProof;
    private Customer customer;

    public Payment(String payOrder, String payMethod, String payAmount, String payProof, Customer customer) {
        this.payOrder = payOrder;
        this.payMethod = payMethod;
        this.payAmount = payAmount;
        this.payProof = payProof;
        this.customer = customer;
    }
}
