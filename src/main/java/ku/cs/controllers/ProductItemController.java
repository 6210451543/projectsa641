package ku.cs.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import ku.cs.model.Product;
import ku.cs.model.WishList;
import ku.cs.service.WishListDataSource;
import org.w3c.dom.events.MouseEvent;

import java.io.IOException;
import java.util.HashMap;

public class ProductItemController {
    private int quantity;
    private Product product;
    private WishList wishList;
    private WishListDataSource dataSource;

    @FXML
    private ImageView productImageView;

    @FXML
    private Label quantityLabel;

    @FXML
    private Label productNameLabel;

    @FXML
    private Label timeLabel;

    @FXML
    private Label productPriceLabel;

    @FXML public void initialize(){
        dataSource = new WishListDataSource("data","wishlist.csv");
        wishList = dataSource.getData();
        Platform.runLater(()->{
            productNameLabel.setText(product.getProductName());
            productPriceLabel.setText(String.valueOf(product.getProductPrice()));
            timeLabel.setText(product.getProductionTime());
            productImageView.setImage(new Image("/ku/cs/image-sa641/1-backdrop.jpg"));
        });

    }
    @FXML
    public void setProduct(Product product){
        this.product = product;
    }

    @FXML
    void handleAddQuantity() {
        quantity = Integer.parseInt(quantityLabel.getText());
        quantity ++;
        quantityLabel.setText(String.valueOf(quantity));
    }

    @FXML
    void handleAddWishlistButton() {
        wishList.addWishlist(product,quantity);
        dataSource.setData(wishList);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ku/cs/fxml/product.fxml"));
        try {
            fxmlLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ProductController pc = fxmlLoader.getController();
        pc.setWishList();
    }

    @FXML
    void handleRemoveQuantity() {
        quantity = Integer.parseInt(quantityLabel.getText());
        quantity --;
        quantityLabel.setText(String.valueOf(quantity));
    }
}
