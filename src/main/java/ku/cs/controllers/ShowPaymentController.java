package ku.cs.controllers;

import javafx.fxml.FXML;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.Customer;
import ku.cs.model.Order;
import ku.cs.model.User;
import ku.cs.service.DatabaseConnection;
import org.w3c.dom.Text;

public class ShowPaymentController {

    DatabaseConnection connectNow = new DatabaseConnection();
    PreparedStatement preparedStatement;
    Connection connection = connectNow.getConnection();

    @FXML private TextField orderIdTextField;
    @FXML private TextField methodTextField;
    @FXML private TextField amountTextField;
    @FXML private TextField referenceTextField;


    private User user;
    private Order order;

    @FXML
    public void initialize() {
        setUser((User) FXRouter.getData());
    }

    public void setUser(User user){
        this.user = user;
    }
    public void setOrder(Order order){this.order = order;}


    public void insertPayment(){
        try{
            preparedStatement = connection.prepareStatement("INSERT INTO payment(pay_order,pay_method,pay_amount,pay_proof,customer_username) VALUES (?,?,?,?,?)");
            preparedStatement.setString(1, orderIdTextField.getText());
            preparedStatement.setString(2, methodTextField.getText());
            preparedStatement.setString(3, amountTextField.getText());
            preparedStatement.setString(4, referenceTextField.getText());
            preparedStatement.setString(5, user.getUserName());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML void handleSubmitBtn(){
        if (orderIdTextField.getText().isEmpty() || methodTextField.getText().isEmpty() || amountTextField.getText().isEmpty() ||
        referenceTextField.getText().isEmpty()){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Incomplete payment information");
            alert.setContentText("Please fill out the information completely");
            alert.showAndWait();
        }
        else
        {
            insertPayment();
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Successful");
            alert.setContentText("Register successful");
            alert.showAndWait();
            try {
                FXRouter.goTo("product");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @FXML
    void handleBackButton() {
        try {
            FXRouter.goTo("myOrder",user);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
