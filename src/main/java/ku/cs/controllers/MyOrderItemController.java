package ku.cs.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import ku.cs.model.Customer;
import ku.cs.model.Order;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
public class MyOrderItemController {


    private Order order;
    private Customer customer;
    @FXML
    private Label orderIDLabel;

    @FXML
    private Label orderStatusLabel;

    @FXML
    private Label paymentStatusLabel;

    @FXML
    private Label productionStatusLabel;

    @FXML
    private Button button1;

    @FXML
    private Button button2;

    @FXML
    public void setOrder(Order order){
        this.order = order;
        orderIDLabel.setText(order.getOrderID());
        setPaymentStatus(order.getCheckoutStatus());
        setProductionStatus(order.getProductionStatus());
        setOrderStatus(order.getOrderStatus());
    }

    @FXML
    private void setOrderStatus(String orderStatus){
        button1.setDisable(false);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    FXRouter.goTo("showOrder",customer,order);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        if(orderStatus.equals("Incomplete")){
            orderStatusLabel.setStyle("-fx-text-fill: #fc6238;");
        }else if(orderStatus.equals("Complete")){
            orderStatusLabel.setStyle("-fx-text-fill: #fc6238;");
            button1.setVisible(true);
            button2.setVisible(true);
        }orderStatusLabel.setText("\""+orderStatus+"\"");
    }

    @FXML
    private void setPaymentStatus(String paymentStatus){
        button2.setDisable(false);
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    FXRouter.goTo("showPayment", customer);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        if(paymentStatus.equals("Not paid")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
            button2.setDisable(true);
        }else if(paymentStatus.equals("Waiting")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
        }else if(paymentStatus.equals("Paid")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
        }paymentStatusLabel.setText("\""+paymentStatus+"\"");
    }

    @FXML
    private void setProductionStatus(String productionStatus){
        if(productionStatus.equals("In process")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
        }else if(productionStatus.equals("Finished")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
        }else if (productionStatus.equals("Not enter")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
        }productionStatusLabel.setText("\""+productionStatus+"\"");
    }

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

}
