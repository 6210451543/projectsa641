package ku.cs.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ku.cs.model.Customer;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.Order;
import ku.cs.model.OrderList;
import ku.cs.model.Product;
import ku.cs.service.DatabaseConnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MyOrderController {
    DatabaseConnection connectNow = new DatabaseConnection();
    Connection con = connectNow.getConnection();
    ResultSet rs1 = con.createStatement().executeQuery("select * from orders");
    OrderList orderList;
//    ArrayList<Order> orderArr = new ArrayList<>();

    private Customer customer;


    @FXML
    private MenuButton userMenuButton;

    @FXML
    private ImageView userImageView;

    @FXML
    private VBox myOrderVBox;

    public MyOrderController() throws SQLException {
    }
    @FXML public void initialize() throws SQLException {
        orderList = new OrderList();
        while (rs1.next()){
            orderList.addOrder(new Order(rs1.getString("order_id") ,
                   Double.parseDouble(rs1.getString("total_price")),rs1.getString("customer_username"),
                    rs1.getString("checkout_status"),rs1.getString("production_status"),rs1.getString("order_status")));
        }
        System.out.println(orderList.getOrders().get(0));
        setCustomer((Customer) FXRouter.getData());
        Platform.runLater(()->{
            userMenuButton.setText(customer.getUserName());
        });
        setOrderList();
    }
    @FXML
    private void setOrderList() {
        for(Order order:orderList.getOrders()){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ku/cs/fxml/myOrderItem.fxml"));
            HBox orderItem = null;
            try{
                orderItem = fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            MyOrderItemController moic = fxmlLoader.getController();
            moic.setOrder(order);
            moic.setCustomer(customer);
            myOrderVBox.getChildren().add(orderItem);
        }
    }

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

    @FXML
    void handleBackButton() {
        try {
            FXRouter.goTo("product",customer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @FXML
    void handleLogoutButton() {
        try {
            ku.cs.com.github.saacsos.fxrouter.FXRouter.goTo("login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
