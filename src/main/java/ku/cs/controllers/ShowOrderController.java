package ku.cs.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;

import java.io.IOException;

import javafx.scene.control.Label;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.Order;
import ku.cs.model.User;
import ku.cs.service.OrderHardcodeDataSource;

public class ShowOrderController {
    private User user;
    private Order order;


    @FXML
    private Label orderIDLabel;

    @FXML
    private Label customerUsernameLabel;

    @FXML
    private Label totalPriceLabel;

    @FXML
    private Label paymentStatusLabel;

    @FXML
    private Label productionStatusLabel;

    @FXML
    private Label orderStatusLabel;

    @FXML
    public void initialize() {
        setUser((User) FXRouter.getData());
        setOrder((Order) FXRouter.getData2());
        Platform.runLater(this::setLabel);
    }


    public void setOrder(Order order){
        this.order = order;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @FXML
    private void setLabel(){
        orderIDLabel.setText(order.getOrderID());
        customerUsernameLabel.setText(order.getCustomer());
        totalPriceLabel.setText(String.valueOf(order.getTotalPrice())+" Bath");
        paymentStatusLabel.setText("\""+order.getCheckoutStatus()+"\"");
        productionStatusLabel.setText("\""+order.getProductionStatus()+"\"");
        orderStatusLabel.setText("\""+order.getOrderStatus()+"\"");
    }

    @FXML
    void handleBackButton() {
        if(user.getUserName().equals("admin")){
            try {
                FXRouter.goTo("sales",user);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            try {
                FXRouter.goTo("myOrder",user);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
