package ku.cs.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import ku.cs.model.Order;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.User;

import java.io.IOException;

// green #61ff69
// yellow #ffdf61
// red #ff6961
public class OrderItemController {

    private User user;
    private Order order;
    @FXML
    private Label orderIDLabel;

    @FXML
    private Label orderStatusLabel;

    @FXML
    private Label paymentStatusLabel;

    @FXML
    private Label productionStatusLabel;

    @FXML
    private Button button1;

    @FXML
    private Button button2;

    @FXML
    private Button button3;

    @FXML
    public void setOrder(Order order){
        this.order = order;
        orderIDLabel.setText(order.getOrderID());
        setPaymentStatus(order.getCheckoutStatus());
        setProductionStatus(order.getProductionStatus());
        setOrderStatus(order.getOrderStatus());
    }

    @FXML
    private void setOrderStatus(String orderStatus){
        button1.setDisable(false);
        button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    FXRouter.goTo("showOrder",user);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        if(orderStatus.equals("Incomplete")){
            orderStatusLabel.setStyle("-fx-text-fill: #fc6238;");
        }else if(orderStatus.equals("Complete")){
            orderStatusLabel.setStyle("-fx-text-fill: #fc6238;");
            button3.setDisable(true);
            button3.setVisible(false);
            button1.setVisible(true);
            button2.setVisible(true);
        }orderStatusLabel.setText("\""+orderStatus+"\"");
    }

    @FXML
    private void setPaymentStatus(String paymentStatus){
        button2.setDisable(false);
        button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                try {
                    FXRouter.goTo("showPayment",user);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        if(paymentStatus.equals("Not paid")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
            button2.setDisable(true);
        }else if(paymentStatus.equals("Waiting")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
        }else if(paymentStatus.equals("Paid")){
            paymentStatusLabel.setStyle("-fx-text-fill: #0065a2;");
            button3.setDisable(false);
            button3.setText("Confirm");
            button3.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    //ยืนยันการผลิต
                }
            });
        }paymentStatusLabel.setText("\""+paymentStatus+"\"");
    }

    @FXML
    private void setProductionStatus(String productionStatus){
        if(productionStatus.equals("In process")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
            button3.setDisable(false);
            button3.setText("Report");
            button3.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    //รายงานการผลิต
                }
            });
        }else if(productionStatus.equals("Finished")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
            button1.setVisible(false);
            button2.setVisible(false);
            button3.setDisable(false);
            button3.setText("Closed");
            button3.setOnAction(new EventHandler<ActionEvent>() {
                @Override public void handle(ActionEvent e) {
                    //ปิดการขาย
                }
            });
        }else if (productionStatus.equals("Not enter")){
            productionStatusLabel.setStyle("-fx-text-fill: #006600;");
        }productionStatusLabel.setText("\""+productionStatus+"\"");
    }

    public void setUser(User user){
        this.user = user;
    }
    @FXML
    void handleGoOrderView() {

    }

}
