package ku.cs.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import ku.cs.model.Product;

public class WishListItemController {
    @FXML
    private Label productNameLabel;

    @FXML
    private Label quantityLabel;

    @FXML
    public void setProduct(Product product, int quantity){
        productNameLabel.setText(product.getProductName());
        quantityLabel.setText(quantity + " piece");
    }

}
