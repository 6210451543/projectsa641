package ku.cs.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.Order;
import ku.cs.model.OrderList;
import ku.cs.model.User;
import ku.cs.model.Userlist;
import ku.cs.service.DatabaseConnection;
import ku.cs.service.OrderHardcodeDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SalesController {

    DatabaseConnection connectNow = new DatabaseConnection();
    Connection con = connectNow.getConnection();
    ResultSet rs1 = con.createStatement().executeQuery("select * from orders");

    private User user;
    private OrderList orderList;


    @FXML
    private MenuButton userMenuButton;

    @FXML
    private ImageView userImageView;

    @FXML
    private VBox orderVbox;

    public SalesController() throws SQLException {
    }

    @FXML
    public void initialize() throws SQLException {
        orderList = new OrderList();
        while (rs1.next()){
            orderList.addOrder(new Order(rs1.getString("order_id") ,
                    Double.parseDouble(rs1.getString("total_price")),rs1.getString("customer_username"),
                    rs1.getString("checkout_status"),rs1.getString("production_status"),rs1.getString("order_status")));
        }

        setUser((User) FXRouter.getData());
        Platform.runLater(()->{
            userMenuButton.setText(user.getUserName());
        });
        setOrderList();
    }

    @FXML
    private void setOrderList() {
        for(Order order:orderList.getOrders()){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ku/cs/fxml/orderItem.fxml"));
            HBox orderItem = null;
            try{
                orderItem = fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            OrderItemController oic = fxmlLoader.getController();
            oic.setOrder(order);
            oic.setUser(user);
            orderVbox.getChildren().add(orderItem);
        }
    }

    public void setUser(User user){
        this.user = user;
    }

    @FXML
    void handleLogoutButton() {
        try {
            FXRouter.goTo("login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
