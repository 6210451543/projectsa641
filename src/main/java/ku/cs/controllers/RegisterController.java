package ku.cs.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ku.cs.service.DatabaseConnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;

public class RegisterController {

    DatabaseConnection connectNow = new DatabaseConnection();
    PreparedStatement preparedStatement;
    Connection connection = connectNow.getConnection();


    @FXML private TextField usernameTextField;
    @FXML private PasswordField passwordField;
    @FXML private TextField shippingAddressTextfield;
    @FXML private TextField telTextField;
    @FXML private TextField emailTextField;
    @FXML private TextField firstNameTextField;
    @FXML private TextField lastNameTextField;

    @FXML private Button goSigninButton;

    @FXML private PasswordField cfPasswordField;

    @FXML
    private Button signupUserButton;


    @FXML
    private Label ifSignUpLabel;

    @FXML public void initialize(){
        numericOnly(telTextField);
    }

    @FXML
    void checkPasswordIsDifferent() {

    }

    @FXML
    void checkPasswordMustBeEight() {

    }

    @FXML
    void checkUsernameAlreadyUsed() {

    }

    @FXML
    void handleGoLoginButton() {
        try {
            FXRouter.goTo("login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void insertCustomer() {
        try{
            preparedStatement = connection.prepareStatement("INSERT INTO customer(customer_username,customer_password,customer_address,customer_tel,customer_email,customer_name) VALUES (?,?,?,?,?,?)");
            preparedStatement.setString(1, usernameTextField.getText());
            preparedStatement.setString(2, passwordField.getText());
            preparedStatement.setString(3, shippingAddressTextfield.getText());
            preparedStatement.setString(4, telTextField.getText());
            preparedStatement.setString(5, emailTextField.getText());
            preparedStatement.setString(6, firstNameTextField.getText() + " " +lastNameTextField.getText());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    void handleRegisterButton() {
        if(usernameTextField.getText().trim().isEmpty()  || passwordField.getText().trim().isEmpty() || cfPasswordField.getText().trim().isEmpty() ||
        shippingAddressTextfield.getText().trim().isEmpty() || telTextField.getText().trim().isEmpty() ||
        emailTextField.getText().trim().isEmpty() || firstNameTextField.getText().trim().isEmpty() || lastNameTextField.getText().trim().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Incomplete registration information");
            alert.setContentText("Please fill out the information completely");
            alert.showAndWait();

        }else if(!passwordField.getText().equals(cfPasswordField.getText())){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Password entered is different");
            alert.setContentText("Please confirm your password again");
            alert.showAndWait();
            cfPasswordField.clear();
        }else{
            insertCustomer();
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Successful");
            alert.setContentText("Register successful");
            alert.showAndWait();
            try {
                FXRouter.goTo("login");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void numericOnly(final TextField field) {
        field.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(
                    ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {
                if (!newValue.matches("\\d*")) {
                    field.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }
}
