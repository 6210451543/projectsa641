package ku.cs.controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.MenuButton;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;
import ku.cs.model.Customer;
import ku.cs.model.Order;
import ku.cs.model.Product;
import ku.cs.model.WishList;
import ku.cs.service.DatabaseConnection;
import ku.cs.service.WishListDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductController {

    DatabaseConnection connectNow = new DatabaseConnection();
    Connection con = connectNow.getConnection();
    ResultSet rs = con.createStatement().executeQuery("select * from product");
    ResultSet rs2 = con.createStatement().executeQuery("select * from product");

    private Product product;
    private Customer customer;
    private ArrayList<Product> productArr = new ArrayList<>();
    private WishList wishList;
    int totalProductionTime = 0;
    double totalPrice = 0;
    String idQuery = "select product_id from product";


    @FXML private VBox wishListVbox;
    @FXML private VBox productListVbox;
    @FXML private Label totalPriceLabel;
    @FXML Label productionTimeLabel;
    @FXML private MenuButton userMenuButton;

    @FXML
    private ImageView userImageView;

    public ProductController() throws SQLException {
    }

    @FXML public void initialize() throws SQLException {
        WishListDataSource dataSource = new WishListDataSource("data","wishlist.csv");
        wishList = dataSource.getData();
        while (rs2.next()){
            productArr.add(new Product(rs2.getString("product_id") ,rs2.getString("product_name"), Double.parseDouble(rs2.getString("product_price")),
                    Double.parseDouble(rs2.getString("stock_of_product")),rs2.getString("production_time")));
        }
        System.out.println(productArr);

        setCustomer((Customer) FXRouter.getData());
        Platform.runLater(()->{
            userMenuButton.setText(customer.getUserName());
        });
        setProductListVbox();
        setWishList();
    }

    @FXML
    private void setProductListVbox() {
        for(Product product: productArr){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ku/cs/fxml/productItem.fxml"));
            Pane productItem = null;
            try{
                productItem = fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ProductItemController pic = fxmlLoader.getController();
            pic.setProduct(product);
            productListVbox.getChildren().add(productItem);
        }
    }

    @FXML
    private void setWishListVbox(){
        for(Product product:wishList.getWishList().keySet()){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/ku/cs/fxml/wishListItem.fxml"));
            HBox wishlistItem = null;
            try{
                wishlistItem = fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
            WishListItemController wic = fxmlLoader.getController();
            wic.setProduct(product,wishList.getWishList().get(product));
            wishListVbox.getChildren().add(wishlistItem);
        }
    }

    @FXML
    public void setWishList(){
        setWishListVbox();
        for(Product product:wishList.getWishList().keySet()){
            if(product.getStockOfProduct() < wishList.getWishList().get(product)){
                totalProductionTime += (Integer.parseInt(product.getProductionTime()) * wishList.getWishList().get(product));
            }
            totalPrice += (product.getProductPrice() * wishList.getWishList().get(product));
        }
        productionTimeLabel.setText(totalProductionTime + " Day");
        totalPriceLabel.setText(totalPrice + " Bath");
    }

    public void addWishlist(Product product, int quantity){
        wishList.addWishlist(product,quantity);
    }

    public void setCustomer(Customer customer){
        this.customer = customer;
    }

    @FXML
    void handleLogoutButton() {
        try {
            System.out.println(customer.getCustomerEmail());
            FXRouter.goTo("login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    void handleAddButton() throws SQLException {
        Integer total_time=0;
        while (rs.next()) {

            productionTimeLabel.setText(rs.getString("production_time"));
        }
        System.out.println("added");
    }

    @FXML void handleClearButton() {

    }

    @FXML
    void handleMyOrderButton() {
        try {
            System.out.println(customer.getCustomerEmail());
            FXRouter.goTo("myOrder",customer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

