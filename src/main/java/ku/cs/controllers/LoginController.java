package ku.cs.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import ku.cs.model.Customer;
import ku.cs.model.User;
import ku.cs.model.Userlist;
import ku.cs.service.DatabaseConnection;
import ku.cs.service.UserHardcodeDataSource;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginController {

    private UserHardcodeDataSource userData;
    private Userlist users;
    int count=0;
    int count_check=0;


    @FXML
    private TextField userNameTextField;

    @FXML
    private PasswordField userPasswordField;

    @FXML
    private Button signinUserButton;

    @FXML
    private Label signupLabel;

    public LoginController() throws SQLException {
    }

    @FXML public void initialize(){

        userData = new UserHardcodeDataSource();
        users = userData.readData();
    }

    @FXML
    void handleGoRegisterButton() {
        try {
            FXRouter.goTo("register");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @FXML
    void handleLoginButton() throws SQLException {
        DatabaseConnection connectNow = new DatabaseConnection();
        Connection con = connectNow.getConnection();
        ResultSet rs = con.createStatement().executeQuery("select * from customer");
        ResultSet ck = con.createStatement().executeQuery("select * from customer");
        while (ck.next()){
            count_check+=1;
        }

        String username=userNameTextField.getText();
        String password=userPasswordField.getText();

        if (!username.isEmpty() && !password.isEmpty()) {
            if (username.equals("admin") && password.equals("admin")) {
                try {
                    FXRouter.goTo("sales",new User("admin","admin"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                while (rs.next()) {
                    count+=1;
                    String address=rs.getString("customer_address");
                    String tel=rs.getString("customer_tel");
                    String email=rs.getString("customer_email");
                    String name=rs.getString("customer_name");

                    if (username.equals(rs.getString("customer_username")) && password.equals(rs.getString("customer_password"))) {
                        try {
                            FXRouter.goTo("product",new Customer(username,password,address,tel,email,name));
                            break;
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("");
                alert.setHeaderText("Invalid Username or Password");
                alert.setContentText("Please input valid username or password");
                alert.showAndWait();
        }
        if (count == count_check){
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("");
            alert.setHeaderText("Invalid Username or Password");
            alert.setContentText("Please input valid username or password");
            alert.showAndWait();

        }

        System.out.println("count check " + count_check);
        count_check=0;
        System.out.println("count main loop " + count);
        count=0;
    }
}

