package ku.cs.controllers;

import javafx.event.ActionEvent;
import ku.cs.com.github.saacsos.fxrouter.FXRouter;

import java.io.IOException;

public class MainControllers {

    public void handleStartButton() {
        try {
            FXRouter.goTo("login");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
